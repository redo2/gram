from django import template
from django.template.defaultfilters import stringfilter
import requests

register = template.Library()


@register.filter
@stringfilter
def upper(value):
    if ',' in value:
        return value.split(',')[0].upper() + ' AGO'
    else:
        return value.upper()


upper.is_safe = True


from django import template
from django.template.defaultfilters import stringfilter
import requests

register = template.Library()


@register.filter
@stringfilter
def get_media_type(value):
    try:
        response = requests.head(value)
        content_type = response.headers['Content-Type']
        if content_type.startswith('video'):
            return 'video'
        elif content_type.startswith('image'):
            return 'image'
        else:
            return None
    except Exception as e:
        # Handle exceptions (e.g., connection error, invalid URL, etc.) if necessary
        print(e)
        return None


get_media_type.is_safe = True
