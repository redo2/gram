# Generated by Django 4.2.6 on 2023-10-13 18:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('homepage', '0012_auto_20190412_1128'),
    ]

    operations = [
        migrations.AlterField(
            model_name='postimage',
            name='modelimage',
            field=models.FileField(upload_to='post_images'),
        ),
    ]
